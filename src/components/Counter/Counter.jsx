import { useEffect, useState } from "react";
import { OtherComponent } from '../../components';


const Counter = () => {
    const [counter, setCounter] = useState(0);

    useEffect(() => {
        if(counter % 2 === 0) {
            console.log('El contador es Par');
        } else {
            console.log('El contador es Impar');
        };

        return () => {
            console.log('Valor anterior del contador: ', counter);
        };
    },[counter])

    return (
        <div>
            <button onClick={() => setCounter(counter + 1)}>+</button>
            <button onClick={() => setCounter(counter - 1)}>-</button>
            <div style={{ fontSize: '52px', color: 'yellow', backgroundColor: 'lightblue' }}>
                Counter: {counter}
            </div>
            <OtherComponent />
        </div>
    )
}

export default Counter;
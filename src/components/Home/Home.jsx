import { useContext, useEffect } from "react"
import { UserContext } from "../../App"


const Home = () => {
    const user = useContext(UserContext);

    useEffect(() => {
        const scrollHandler = () => {
            console.log(`Scrolling at: ${window.scrollY}`);
        }

        document.addEventListener('scroll', scrollHandler);

        return () => {
            document.removeEventListener('scroll', scrollHandler);
        };
    }, []);

    return (
        <div className='home'>
            <div>Esto es Home, nombre actual: {user.name}</div>
        </div>
    );
};

export default Home;
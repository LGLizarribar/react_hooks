import { useEffect, useReducer } from "react";


const INITIAL_STATE = {
    characters: [],
    error: null,
    isLoading: false,
};

const SET_CHARACTERS = 'SET_CHARACTERS';
const SET_ERROR = 'SET_ERROR';
const SET_IS_LOADING = 'SET_IS_LOADING';

const reducer = (state, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_CHARACTERS:
            return {...state, characters: payload, isLoading: false};
        case SET_ERROR:
            return {...state, error: payload, isLoading: false};
        case SET_IS_LOADING:
            return {...state, isLoading: payload};
        default:
            return state;
    };
};

const RickYMorty = () => {
    const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
    const { characters, error, isLoading } = state;

    useEffect(() => {
        dispatch({type: SET_IS_LOADING, payload: true});

        fetch('https://rickandmortyapi.com/api/character/?page=2')
            .then((res) => res.json())
            .then((data) => {
                if (!data.error) {
                    dispatch({ type: SET_CHARACTERS, payload: data.results });
                } else {
                    throw new Error(data.error);
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: SET_ERROR, payload: error.message });
            });
    }, []);

    return (
        <div>
            {isLoading ? (
                <div>Cargando...</div>
            ) : (
                <div>
                    {characters.length ? (
                        <div>Personajes cargados correctamente</div>
                    ) : null}
                    {error ? <div>{error}</div> : null}
                </div>
            )}
        </div>
    );
};

export default RickYMorty;
import Home from './Home/Home';
import Counter from './Counter/Counter';
import OtherComponent from './OtherComponent/OtherComponent';
import RickYMorty from './RickYMorty/RickYMorty';

export {
    Home,
    Counter,
    OtherComponent,
    RickYMorty,
}

import { useContext, useState } from "react"
import { UserContext } from "../../App"


const OtherComponent = () => {
    const user = useContext(UserContext);
    const [data, setData] = useState({});

    const handleInput = ev => {
        const {name, value} = ev.target;

        setData({...data, [name]: value});
    };

    const submitForm = (ev) => {
        ev.preventDefault();
        user.handleUser(data);
        setData({});
    };

    return (
        <div>
            <div>Cambiar usuario</div>
            <h1>Nombre actual: {user.name}</h1>
            <form onSubmit={submitForm}>
                <input type='text' onChange={handleInput} name='name' placeholder='Nombre' />
                <input type='email' onChange={handleInput} name='email' placeholder='Email' />
                <input type='company' onChange={handleInput} name='company' placeholder='Empresa' />

                <button type='submit'>Enviar</button>
            </form>
        </div>
    );
};

export default OtherComponent;
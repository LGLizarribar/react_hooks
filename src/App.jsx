import { createContext, useState } from 'react';
import { Home, Counter, RickYMorty } from './components';
import './App.scss';

const INITIAL_STATE = {
  name: 'Laura García',
  email: 'laura@upgrade.com',
  company: 'Laura, S.L.',
};

export const UserContext = createContext(null);

const App = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [user, setUser] = useState(INITIAL_STATE);

  const handleUser = (newUser) => {
    setUser({...user, ...newUser});
  }

  return (
    <UserContext.Provider value={{
      ...user,
      handleUser,
    }}>
      <div className="app">
        APP
        <button onClick={() => setIsOpen(!isOpen)}>Montar Componente Home</button>
        {isOpen && <Home />}
        <Counter />
        <RickYMorty />
      </div>
    </UserContext.Provider>
  );
}

export default App;
